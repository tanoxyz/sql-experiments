
USE AdventureWorks2019;

IF (SELECT 1 FROM Production.ProductModel WHERE Name = 'ProductModelName') = 1
	UPDATE Production.ProductModel SET CatalogDescription=null,Instructions=null WHERE Name='ProductionModelName';
ELSE
	INSERT INTO Production.ProductModel (Name, CatalogDescription, Instructions) VALUES('ProductModelName', null, null);

DECLARE @MID int;
SELECT @MID = ProductModelID FROM Production.ProductModel WHERE Name = 'ProductModelName';

IF (SELECT 1 FROM Production.Product WHERE ProductNumber = 'P33') = 1
	UPDATE Production.Product SET
		Name='ProductName',
		SafetyStockLevel=1000,
		ReorderPoint = 100,
		StandardCost = 0,
		ListPrice = 0,
		DaysToManufacture = 1,
		SellStartDate = GETDATE(),
		ProductModelID = @MID
		WHERE ProductNumber = 'P33';
ELSE
	INSERT INTO Production.Product (Name, ProductNumber, SafetyStockLevel, ReorderPoint, StandardCost, ListPrice, DaysToManufacture, SellStartDate, ProductModelID)  
		VALUES('ProductName', 'P33', 1000, 100, 0, 0, 1, GETDATE(), @MID);


