#include <stdio.h>

void
BASIC_SP_PER_ROW(int lim) {
    printf (
    "\n"
    "DROP PROCEDURE IF EXISTS #InsertProductsAndModels\n"
    "\n"
    "\n"
    "GO\n"
    "\n"
    "\n"
    "CREATE PROCEDURE #InsertProductsAndModels   \n"
    "    @ProductModelName nvarchar(50),   \n"
    "    @CatalogDescription xml,\n"
    "\t@Instructions xml,\n"
    "\t@ProductName nvarchar(50),\n"
    "\t@ProductNumber nvarchar(25),\n"
    "\t@SafetyStockLevel smallint,\n"
    "\t@ReorderPoint smallint,\n"
    "\t@StandardCost money,\n"
    "\t@ListPrice money,\n"
    "\t@DaysToManufacture int\n"
    "AS\n"
    "\n"
    "SET NOCOUNT ON;\n"
    "\n"
    "IF (SELECT 1 FROM Production.ProductModel WHERE Name = @ProductModelName) = 1\n"
    "\tUPDATE Production.ProductModel SET CatalogDescription=@CatalogDescription,Instructions=@Instructions WHERE Name=@ProductModelName;\n"
    "ELSE\n"
    "\tINSERT INTO Production.ProductModel (Name, CatalogDescription, Instructions) VALUES(@ProductModelName, @CatalogDescription, @Instructions);\n"
    "\n"
    "DECLARE @MID int;\n"
    "SELECT @MID = ProductModelID FROM Production.ProductModel WHERE Name = @ProductModelName;\n"
    "\n"
    "IF (SELECT 1 FROM Production.Product WHERE ProductNumber = @ProductNumber) = 1\n"
    "\tUPDATE Production.Product SET\n"
    "\t\tName=@ProductName,\n"
    "\t\tSafetyStockLevel=@SafetyStockLevel,\n"
    "\t\tReorderPoint = @ReorderPoint,\n"
    "\t\tStandardCost = @StandardCost,\n"
    "\t\tListPrice = @ListPrice,\n"
    "\t\tDaysToManufacture = @DaysToManufacture,\n"
    "\t\tSellStartDate = GETDATE(),\n"
    "\t\tProductModelID = @MID\n"
    "\t\tWHERE ProductNumber = @ProductNumber;\n"
    "ELSE\n"
    "\tINSERT INTO Production.Product (Name, ProductNumber, SafetyStockLevel, ReorderPoint, StandardCost, ListPrice, DaysToManufacture, SellStartDate, ProductModelID)  \n"
    "\t\tVALUES(@ProductName, @ProductNumber, @SafetyStockLevel, @ReorderPoint, @StandardCost, @ListPrice, @DaysToManufacture, GETDATE(), @MID);\n"
    "\n"
    "\n"
    "\n"
    "GO\n\n"
    );
    char *sep = "";
    for (int i = 0; i < lim; i+=1) {
        printf("EXEC #InsertProductsAndModels N'PMODEL%d', null, null, N'P%d', N'PCODE%d', 1000, 100, 0, 0, 1;\n", i, i, i);
    }
}

void
SP_WITH_TVP() {
    printf(
    "DROP PROCEDURE IF EXISTS InsertProductsAndModelsTVP\n"
    "DROP TYPE IF EXISTS InsertProductsAndModelsType\n"
    "GO\n"
    "\n"
    "CREATE TYPE InsertProductsAndModelsType \n"
    "   AS TABLE ( \n"
    "    ProductModelName nvarchar(50),   \n"
    "    CatalogDescription xml,\n"
    "\tInstructions xml,\n"
    "\tProductName nvarchar(50),\n"
    "\tProductNumber nvarchar(25),\n"
    "\tSafetyStockLevel smallint,\n"
    "\tReorderPoint smallint,\n"
    "\tStandardCost money,\n"
    "\tListPrice money,\n"
    "\tDaysToManufacture int)\n"
    "GO\n"
    "\n"
    "\n"
    "CREATE PROCEDURE InsertProductsAndModelsTVP @tvp InsertProductsAndModelsType READONLY AS\n"
    "\n"
    "\tSET NOCOUNT ON;\n"
    "\n"
    "\t-- Insert or update all product models at once\n"
    "    MERGE Production.ProductModel AS destination\n"
    "    USING @tvp AS source  \n"
    "    ON (destination.Name = source.ProductModelName)  \n"
    "    WHEN MATCHED THEN\n"
    "\t\tUPDATE SET CatalogDescription=source.CatalogDescription, Instructions=source.Instructions \n"
    "    WHEN NOT MATCHED THEN  \n"
    "        INSERT (Name, CatalogDescription, Instructions) VALUES(source.ProductModelName, source.CatalogDescription, source.Instructions);\n"
    "\n"
    "\t-- Insert or update all products at once\n"
    "\tMERGE Production.Product AS destination\n"
    "\t-- The using table has to contain a join with ProductModel in order to get the ProductModelId\n"
    "\tUSING (SELECT tvp.*, ppm.ProductModelId FROM @tvp AS tvp JOIN Production.ProductModel AS ppm ON tvp.ProductModelName = ppm.Name) AS source\n"
    "\tON (destination.ProductNumber = source.ProductNumber)\n"
    "\tWHEN MATCHED THEN\n"
    "\t\tUPDATE SET\n"
    "\t\t\tName=source.ProductName,\n"
    "\t\t\tSafetyStockLevel=source.SafetyStockLevel,\n"
    "\t\t\tReorderPoint = source.ReorderPoint,\n"
    "\t\t\tStandardCost = source.StandardCost,\n"
    "\t\t\tListPrice = source.ListPrice,\n"
    "\t\t\tDaysToManufacture = source.DaysToManufacture,\n"
    "\t\t\tSellStartDate = GETDATE(),\n"
    "\t\t\tProductModelID = source.ProductModelId\n"
    "\tWHEN NOT MATCHED THEN\n"
    "\t\tINSERT (Name, ProductNumber, SafetyStockLevel, ReorderPoint, StandardCost, ListPrice, DaysToManufacture, SellStartDate, ProductModelID) VALUES(\n"
    "\t\t\tsource.ProductName,\n"
    "\t\t\tsource.ProductNumber,\n"
    "\t\t\tsource.SafetyStockLevel,\n"
    "\t\t\tsource.ReorderPoint,\n"
    "\t\t\tsource.StandardCost,\n"
    "\t\t\tsource.ListPrice,\n"
    "\t\t\tsource.DaysToManufacture,\n"
    "\t\t\tGETDATE(),\n"
    "\t\t\tsource.ProductModelId);\n"
    "\n"
    "GO"
    );
}

void
MERGE_TVAR() {
    printf(
    "\n\n\n-- Insert or update all product models at once\n"
    "MERGE Production.ProductModel AS destination\n"
    "USING @tmp AS source  \n"
    "ON (destination.Name = source.ProductModelName)  \n"
    "WHEN MATCHED THEN\n"
    "\tUPDATE SET CatalogDescription=source.CatalogDescription, Instructions=source.Instructions \n"
    "WHEN NOT MATCHED THEN  \n"
    "    INSERT (Name, CatalogDescription, Instructions) VALUES(source.ProductModelName, source.CatalogDescription, source.Instructions);\n"
    "\n"
    "-- Insert or update all products at once\n"
    "MERGE Production.Product AS destination\n"
    "-- The using table has to contain a join with ProductModel in order to get the ProductModelId\n"
    "USING (SELECT tmp.*, ppm.ProductModelId FROM @tmp AS tmp JOIN Production.ProductModel AS ppm ON tmp.ProductModelName = ppm.Name) AS source\n"
    "ON (destination.ProductNumber = source.ProductNumber)\n"
    "WHEN MATCHED THEN\n"
    "\tUPDATE SET\n"
    "\t\tName=source.ProductName,\n"
    "\t\tSafetyStockLevel=source.SafetyStockLevel,\n"
    "\t\tReorderPoint = source.ReorderPoint,\n"
    "\t\tStandardCost = source.StandardCost,\n"
    "\t\tListPrice = source.ListPrice,\n"
    "\t\tDaysToManufacture = source.DaysToManufacture,\n"
    "\t\tSellStartDate = GETDATE(),\n"
    "\t\tProductModelID = source.ProductModelId\n"
    "WHEN NOT MATCHED THEN\n"
    "\tINSERT (Name, ProductNumber, SafetyStockLevel, ReorderPoint, StandardCost, ListPrice, DaysToManufacture, SellStartDate, ProductModelID) VALUES(\n"
    "\t\tsource.ProductName,\n"
    "\t\tsource.ProductNumber,\n"
    "\t\tsource.SafetyStockLevel,\n"
    "\t\tsource.ReorderPoint,\n"
    "\t\tsource.StandardCost,\n"
    "\t\tsource.ListPrice,\n"
    "\t\tsource.DaysToManufacture,\n"
    "\t\tGETDATE(),\n"
    "\t\tsource.ProductModelId);\n"
    );
}

void
TVAR_INSERT(int lim) {
    printf ("\n\nSET NOCOUNT ON;\n\n");
    printf(
    "DECLARE @tmp AS TABLE (\n"
    "    ProductModelName nvarchar(50),\n"
    "    CatalogDescription xml,\n"
    "\tInstructions xml,\n"
    "\tProductName nvarchar(50),\n"
    "\tProductNumber nvarchar(25),\n"
    "\tSafetyStockLevel smallint,\n"
    "\tReorderPoint smallint,\n"
    "\tStandardCost money,\n"
    "\tListPrice money,\n"
    "\tDaysToManufacture int)\n\n"
    );
    for (int i = 0; i < lim; i+=1) {
        printf("INSERT INTO @tmp VALUES (N'PMODEL%d', null, null, N'P%d', N'PCODE%d', 1000, 100, 0, 0, 1)\n", i, i, i);
    }
    MERGE_TVAR();
    printf ("\n\nSET NOCOUNT OFF;\n");
}

void
TVAR_INSERT_VALUES(int lim) {
    printf ("\n\nSET NOCOUNT ON;\n\n");
    printf(
    "DECLARE @tmp AS TABLE (\n"
    "    ProductModelName nvarchar(50),\n"
    "    CatalogDescription xml,\n"
    "\tInstructions xml,\n"
    "\tProductName nvarchar(50),\n"
    "\tProductNumber nvarchar(25),\n"
    "\tSafetyStockLevel smallint,\n"
    "\tReorderPoint smallint,\n"
    "\tStandardCost money,\n"
    "\tListPrice money,\n"
    "\tDaysToManufacture int)\n\n"
    );

    const char *sep = "";
    for (int i = 0; i < lim; i+=1) {
        if ((i % 1000) == 0) {
            printf("\nINSERT INTO @tmp VALUES \n");
            sep = "";
        }
        printf("%s(N'PMODEL%d', null, null, N'P%d', N'PCODE%d', 1000, 100, 0, 0, 1)", sep, i, i, i);
        sep = ",\n";
    }
    MERGE_TVAR();
    printf ("\n\nSET NOCOUNT OFF;\n");
}


void
TVAR_INSERT_EXEC(int lim) {
    printf ("\n\nSET NOCOUNT ON;\n\n");
    printf(
    "DECLARE @tmp AS TABLE (\n"
    "    ProductModelName nvarchar(50),\n"
    "    CatalogDescription xml,\n"
    "\tInstructions xml,\n"
    "\tProductName nvarchar(50),\n"
    "\tProductNumber nvarchar(25),\n"
    "\tSafetyStockLevel smallint,\n"
    "\tReorderPoint smallint,\n"
    "\tStandardCost money,\n"
    "\tListPrice money,\n"
    "\tDaysToManufacture int)\n\n"
    );

    printf(
    "\n\n"
    "INSERT @tmp \n"
    "EXEC('\n"
    );
    char *sep = "";
    for (int i = 0; i < lim; i+=1) {
        printf("%sSELECT N''PMODEL%d'', null, null, N''P%d'', N''PCODE%d'', 1000, 100, 0, 0, 1", sep, i, i, i);
        sep = "\n";
    }
    printf ("');\n\n");
    MERGE_TVAR();
    printf ("\n\nSET NOCOUNT OFF;\n");
}

void
BULK_INSERT_CSV(int lim) {
    const char *sep = "";
    for (int i = 0; i < lim; i+=1) {
        printf("%s\"PMODEL%d\",,,\"P%d\",\"PCODE%d\",1000,100,0,0,1", sep, i, i, i);
        sep = "\n";
    }
}

void
TVAR_SELECT_XML(int lim) {

    printf(
    "\n"
    "DROP TABLE IF EXISTS #tmp\n"
    "\n"
    "DECLARE @x xml\n"
    "SELECT @x =\n"
    "N'<Rows>\n"
    );
    for (int i = 0; i < lim; i+=1) {
        printf(
        "<Row "
        "ProductModelName=\"PMODEL%d\" "
        "ProductName=\"P%d\" "
        "ProductNumber=\"PCODE%d\" "
        "SafetyStockLevel=\"1000\" "
        "ReorderPoint=\"100\" "
        "StandardCost=\"0\" "
        "ListPrice=\"0\" "
        "DaysToManufacture=\"1\" "
        " />\n",
        i, i, i);
    }
    
    printf(
    "</Rows>'\n"
    "\n"
    "\n"
    "SELECT \n"
    "\tProductModelName = X.Item.value('@ProductModelName[1]', 'nvarchar(50)'),\n"
    "\tCatalogDescription = X.Item.value('@CatalogDescription[1]', 'nvarchar(max)'),\n"
    "\tInstructions = X.Item.value('@Instructions[1]', 'nvarchar(max)'),\n"
    "\tProductName = X.Item.value('@ProductName[1]', 'nvarchar(50)'),\n"
    "\tProductNumber = X.Item.value('@ProductNumber[1]', 'nvarchar(25)'),\n"
    "\tSafetyStockLevel = X.Item.value('@SafetyStockLevel[1]', 'smallint'),\n"
    "\tReorderPoint = X.Item.value('@ReorderPoint[1]', 'smallint'),\n"
    "\tStandardCost = X.Item.value('@StandardCost[1]', 'money'),\n"
    "\tListPrice = X.Item.value('@ListPrice[1]', 'money'),\n"
    "\tDaysToManufacture = X.Item.value('@DaysToManufacture[1]', 'int')\n"
    "INTO #tmp\n"
    "FROM @x.nodes('/Rows/Row') AS X(Item);"
    );
}



int
main() {
    //printf("USE AdventureWorks2019;\n\n");
    //TVAR_INSERT_EXEC(50000);
    BULK_INSERT_CSV(50000);
    return 0;
}





