using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using Microsoft.SqlServer.Server;


class MainTVP {


private static void Main() {

   string drop_table_type_cmd = @"
DROP TYPE IF EXISTS InsertProductsAndModelsType
";

   string setup_table_type_cmd = @"
CREATE TYPE InsertProductsAndModelsType 
   AS TABLE ( 
    ProductModelName nvarchar(50),   
    CatalogDescription xml,
	Instructions xml,
	ProductName nvarchar(50),
	ProductNumber nvarchar(25),
	SafetyStockLevel smallint,
	ReorderPoint smallint,
	StandardCost money,
	ListPrice money,
	DaysToManufacture int)
";

   string merge_data_cmd = @"
SET NOCOUNT ON;

-- Insert or update all product models at once
MERGE Production.ProductModel AS destination
USING @tvp AS source  
ON (destination.Name = source.ProductModelName)  
WHEN MATCHED THEN
	UPDATE SET CatalogDescription=source.CatalogDescription, Instructions=source.Instructions 
WHEN NOT MATCHED THEN  
    INSERT (Name, CatalogDescription, Instructions) VALUES(source.ProductModelName, source.CatalogDescription, source.Instructions);

-- Insert or update all products at once
MERGE Production.Product AS destination
-- The using table has to contain a join with ProductModel in order to get the ProductModelId
USING (SELECT tvp.*, ppm.ProductModelId FROM @tvp AS tvp JOIN Production.ProductModel AS ppm ON tvp.ProductModelName = ppm.Name) AS source
ON (destination.ProductNumber = source.ProductNumber)
WHEN MATCHED THEN
	UPDATE SET
		Name=source.ProductName,
		SafetyStockLevel=source.SafetyStockLevel,
		ReorderPoint = source.ReorderPoint,
		StandardCost = source.StandardCost,
		ListPrice = source.ListPrice,
		DaysToManufacture = source.DaysToManufacture,
		SellStartDate = GETDATE(),
		ProductModelID = source.ProductModelId
WHEN NOT MATCHED THEN
	INSERT (Name, ProductNumber, SafetyStockLevel, ReorderPoint, StandardCost, ListPrice, DaysToManufacture, SellStartDate, ProductModelID) VALUES(
		source.ProductName,
		source.ProductNumber,
		source.SafetyStockLevel,
		source.ReorderPoint,
		source.StandardCost,
		source.ListPrice,
		source.DaysToManufacture,
		GETDATE(),
		source.ProductModelId);
";

    
    // Table structure in C#
    SqlMetaData[] InsertProductsAndModelsType =
    {
        new SqlMetaData("ProductModelName", SqlDbType.NVarChar, 50),
        new SqlMetaData("CatalogDescription", SqlDbType.Xml),
        new SqlMetaData("Instructions", SqlDbType.Xml),
        new SqlMetaData("ProductName", SqlDbType.NVarChar, 50),
        new SqlMetaData("ProductNumber", SqlDbType.NVarChar, 25),
        new SqlMetaData("SafetyStockLevel", SqlDbType.SmallInt),
        new SqlMetaData("ReorderPoint", SqlDbType.SmallInt),
        new SqlMetaData("StandardCost", SqlDbType.Money),
        new SqlMetaData("ListPrice", SqlDbType.Money),
        new SqlMetaData("DaysToManufacture", SqlDbType.Int)
    };
    // Populate all the data
    var productsAndModelsData = new List<SqlDataRecord>();
    for (int i = 0; i < 50000; i+=1) {
        var currentRow = new SqlDataRecord(InsertProductsAndModelsType);
        var productModelName = System.String.Format("PMODEL{0}", i);
        var productName = System.String.Format("P{0}", i);
        var productNumber = System.String.Format("PCODE{0}", i);
        currentRow.SetValues(productModelName, new SqlXml(), new SqlXml(), productName, productNumber, (Int16)(1000), (Int16)(100), new SqlMoney(0), new SqlMoney(0), (Int32)(1));
        productsAndModelsData.Add(currentRow);
    }

    // Start the connection
    SqlConnection cn = new SqlConnection("server=tcp:127.0.0.1,1433;Initial Catalog=AdventureWorks2019;Integrated Security=false; User=sa; Password=1234ABC?");
    cn.Open();

    var sw = new System.Diagnostics.Stopwatch();
    sw.Start();
    
    // Setup the type in the database
    SqlCommand setup_cmd = new SqlCommand(drop_table_type_cmd, cn);
    setup_cmd.ExecuteNonQuery();
    setup_cmd = new SqlCommand(setup_table_type_cmd, cn);
    setup_cmd.ExecuteNonQuery();

    // Insert all the data
    SqlCommand insertCommand = new SqlCommand(merge_data_cmd, cn);  
    SqlParameter tvpParam = insertCommand.Parameters.AddWithValue("@tvp", productsAndModelsData);  
    tvpParam.SqlDbType = SqlDbType.Structured;  
    tvpParam.TypeName = "InsertProductsAndModelsType";  
    insertCommand.ExecuteNonQuery();

    cn.Close();

    sw.Stop();

    System.Console.WriteLine("Elapsed={0}",sw.Elapsed); 
}


}

