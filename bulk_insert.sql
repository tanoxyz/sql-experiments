
USE AdventureWorks2019;

SET NOCOUNT ON;

DROP TABLE IF EXISTS #tmp

CREATE TABLE #tmp ( 
    ProductModelName nvarchar(50),   
    CatalogDescription xml,
	Instructions xml,
	ProductName nvarchar(50),
	ProductNumber nvarchar(25),
	SafetyStockLevel smallint,
	ReorderPoint smallint,
	StandardCost money,
	ListPrice money,
	DaysToManufacture int)

-- Bulk insert all the data into a temporary table
BULK INSERT #tmp FROM '/bulk_insert.csv' WITH (FORMAT = 'CSV');

-- Insert or update all product models at once
MERGE Production.ProductModel AS destination
USING #tmp AS source  
ON (destination.Name = source.ProductModelName)  
WHEN MATCHED THEN
	UPDATE SET CatalogDescription=source.CatalogDescription, Instructions=source.Instructions 
WHEN NOT MATCHED THEN  
    INSERT (Name, CatalogDescription, Instructions) VALUES(source.ProductModelName, source.CatalogDescription, source.Instructions);

-- Insert or update all products at once
MERGE Production.Product AS destination
-- The using table has to contain a join with ProductModel in order to get the ProductModelId
USING (SELECT tmp.*, ppm.ProductModelId FROM #tmp AS tmp JOIN Production.ProductModel AS ppm ON tmp.ProductModelName = ppm.Name) AS source
ON (destination.ProductNumber = source.ProductNumber)
WHEN MATCHED THEN
	UPDATE SET
		Name=source.ProductName,
		SafetyStockLevel=source.SafetyStockLevel,
		ReorderPoint = source.ReorderPoint, 
		StandardCost = source.StandardCost,
		ListPrice = source.ListPrice,
		DaysToManufacture = source.DaysToManufacture,
		SellStartDate = GETDATE(),
		ProductModelID = source.ProductModelId
WHEN NOT MATCHED THEN
	INSERT (Name, ProductNumber, SafetyStockLevel, ReorderPoint, StandardCost, ListPrice, DaysToManufacture, SellStartDate, ProductModelID) VALUES(
		source.ProductName,
		source.ProductNumber,
		source.SafetyStockLevel,
		source.ReorderPoint,
		source.StandardCost,
		source.ListPrice,
		source.DaysToManufacture,
		GETDATE(),
		source.ProductModelId);

SET NOCOUNT OFF;




