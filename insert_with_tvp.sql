
DROP PROCEDURE IF EXISTS InsertProductsAndModelsTVP
DROP TYPE IF EXISTS InsertProductsAndModelsType
GO

CREATE TYPE InsertProductsAndModelsType 
   AS TABLE ( 
    ProductModelName nvarchar(50),   
    CatalogDescription xml,
	Instructions xml,
	ProductName nvarchar(50),
	ProductNumber nvarchar(25),
	SafetyStockLevel smallint,
	ReorderPoint smallint,
	StandardCost money,
	ListPrice money,
	DaysToManufacture int)
GO


CREATE PROCEDURE InsertProductsAndModelsTVP @tvp InsertProductsAndModelsType READONLY AS

	SET NOCOUNT ON;

	-- Insert or update all product models at once
    MERGE Production.ProductModel AS destination
    USING @tvp AS source  
    ON (destination.Name = source.ProductModelName)  
    WHEN MATCHED THEN
		UPDATE SET CatalogDescription=source.CatalogDescription, Instructions=source.Instructions 
    WHEN NOT MATCHED THEN  
        INSERT (Name, CatalogDescription, Instructions) VALUES(source.ProductModelName, source.CatalogDescription, source.Instructions);

	-- Insert or update all products at once
	MERGE Production.Product AS destination
	-- The using table has to contain a join with ProductModel in order to get the ProductModelId
	USING (SELECT tvp.*, ppm.ProductModelId FROM @tvp AS tvp JOIN Production.ProductModel AS ppm ON tvp.ProductModelName = ppm.Name) AS source
	ON (destination.ProductNumber = source.ProductNumber)
	WHEN MATCHED THEN
		UPDATE SET
			Name=source.ProductName,
			SafetyStockLevel=source.SafetyStockLevel,
			ReorderPoint = source.ReorderPoint,
			StandardCost = source.StandardCost,
			ListPrice = source.ListPrice,
			DaysToManufacture = source.DaysToManufacture,
			SellStartDate = GETDATE(),
			ProductModelID = source.ProductModelId
	WHEN NOT MATCHED THEN
		INSERT (Name, ProductNumber, SafetyStockLevel, ReorderPoint, StandardCost, ListPrice, DaysToManufacture, SellStartDate, ProductModelID) VALUES(
			source.ProductName,
			source.ProductNumber,
			source.SafetyStockLevel,
			source.ReorderPoint,
			source.StandardCost,
			source.ListPrice,
			source.DaysToManufacture,
			GETDATE(),
			source.ProductModelId);

GO

SET NOCOUNT ON;

DECLARE @tmp AS InsertProductsAndModelsType;
INSERT INTO @tmp VALUES ('PMODEL0', '', '', 'P0', 'PCODE0', 10000, 100, 0, 0, 1);
INSERT INTO @tmp VALUES ('PMODEL1', '', '', 'P1', 'PCODE1', 1000, 100, 0, 0, 1);

SET NOCOUNT OFF;

EXEC InsertProductsAndModelsTVP @tmp;

