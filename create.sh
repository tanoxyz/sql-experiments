#!/bin/sh

docker container rm -f sqlserver
docker run -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=1234ABC?" -p 127.0.0.1:1433:1433 --name sqlserver --hostname sqlserver -d mcr.microsoft.com/mssql/server:2019-latest

echo "Copy backup"
docker cp AdventureWorks2019.bak sqlserver:/AdventureWorks2019.bak
docker cp restore.sql sqlserver:/restore.sql

docker container exec -i sqlserver opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P 1234ABC? -i //restore.sql

